package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    // Creating a user
    void createUser(User user);
    // viewing all user
    Iterable<User> getUsername();
    // Delete a user
    ResponseEntity deleteUser(Long id);
    // Update a user
    ResponseEntity updateUser(Long id, User user);
}
